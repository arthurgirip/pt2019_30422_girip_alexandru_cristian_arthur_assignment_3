package Data_Access;

import Model.Product;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class productDAO extends genericDAO {

    public void productDAO(){}

    public static void insertObject(Object o, Connection con) throws IntrospectionException, SQLException, IllegalAccessException {
//        super.insertObject(instance);
        int id = 0;
        String name = "";
        int price = 0;
        String description ="";
        int stoc = 0;

        for (Field field : o.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.getName() == "id")
                    id = (int) field.get(o);
                else if (field.getName() == "name")
                    name = (String) field.get(o);
                else if (field.getName() == "price")
                    price = (int) field.get(o);
                else if (field.getName() == "description")
                    description = (String) field.get(o);
                else if (field.getName() == "stoc")
                    stoc = (int) field.get(o);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        String query = "INSERT INTO product" + " VALUES(" + id + ", '" + name + "', " + price + ",'" +description+"'," + stoc +");";
        Statement stmt = con.createStatement();
        stmt.execute(query);
        stmt.close();
    }
}
