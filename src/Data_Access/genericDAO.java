package Data_Access;

import java.beans.IntrospectionException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

public class genericDAO<T>
{
    protected static final Logger LOGGER = Logger.getLogger(genericDAO.class.getName());

    //private static final Class<> type;

    public genericDAO()
    {

    }


    public static void insertObject(Object o) throws SQLException, IntrospectionException, IllegalAccessException {
        Connection conn = databaseCon.connect();
        PreparedStatement stmt = null;
        if(o.getClass().getName() == "Model.Client")
        {
            clientDAO.insertObject(o, conn);
        }
        else if(o.getClass().getName() == "Model.Product")
        {
            productDAO.insertObject(o, conn);
        }
        else{
            System.out.println(o.getClass().getName());
            orderDAO.insertObject(o, conn);
        }

    }










}
