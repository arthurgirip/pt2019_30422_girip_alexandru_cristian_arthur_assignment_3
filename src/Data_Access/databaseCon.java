package Data_Access;

import java.io.IOException;
import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public class databaseCon{

    private static final Logger LOGGER = Logger.getLogger(databaseCon.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/tema3db?useSSL=false&jdbcCompliantTruncation=false";
    private static final String USER = "root";
    private static final String PASS = "root";

    private static databaseCon singleInstance = new databaseCon();


    public static Connection connect()
    {
        System.out.println("--------Starting Connection--------");

        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        System.out.println("Driver Registered, so far so good!");

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DBURL,USER,PASS);

        } catch (SQLException e) {
            System.out.println("Connection Failed!");
            e.printStackTrace();
            return null;
        }



        if(connection != null) {
            System.out.println("Connected Safely!");
            return connection;
        }
        else {
            System.out.println("Wasn't able to connect!");
            return null;
        }
    }

//    public static Connection getConnection() throws SQLException {
//        return DriverManager.getConnection(DBURL,USER,PASS);
//    }

}
