package Data_Access;

import Model.Client;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class clientDAO extends genericDAO{

    public clientDAO(){}

    public static void insertObject(Object o, Connection con) throws IntrospectionException, SQLException, IllegalAccessException {
//        super.insertObject(instance);
        int id=0;
        String name="";
        String phone="";
        for(Field field : o.getClass().getDeclaredFields())
        {
            field.setAccessible(true);
            try{
                if(field.getName() == "id")
                    id = (int) field.get(o);
                else if(field.getName() == "name")
                    name = (String) field.get(o);
                else if(field.getName() == "phone")
                    phone = (String) field.get(o);
            }catch(IllegalArgumentException e){
                e.printStackTrace();
            }
        }
        System.out.println(phone);
        String query = "INSERT INTO client"+" VALUES("+id+", '"+name+"', '"+phone+"');";
        Statement stmt = con.createStatement();
        stmt.execute(query);
        stmt.close();
    }
}
