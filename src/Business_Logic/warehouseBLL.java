package Business_Logic;

import Data_Access.databaseCon;
import Data_Access.genericDAO;
import Data_Access.productDAO;
import Model.Client;
import Model.Order;
import Model.Product;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class warehouseBLL<T> {

    public static ArrayList<Client> clients = new ArrayList<Client>();
    private ArrayList<Order> orders;
    private ArrayList<Product> products;
    private static Object clientDAO;
    private Object productDAO;


    public warehouseBLL() {

    }

    public static ArrayList<Client> getClients() {
        return clients;
    }

    public ArrayList<Product> getProducts() {
        return this.products;
    }

    public ArrayList<Order> getOrders() {
        return this.orders;
    }


    public static void getClients(ResultSet rs) throws SQLException {
        while (rs.next()) {
            StringBuffer buffer = new StringBuffer();
            buffer.append(" Client ID " + rs.getInt("id") + " ");
            buffer.append(rs.getString("name") + " ");
            buffer.append(rs.getString("phone") + " ");

            System.out.println(buffer.toString());
        }
    }

    public static void addClient(int id, String name, String phone) throws IntrospectionException, SQLException, IllegalAccessException {
        Client c = new Client(id, name, phone);
        clients.add(c);
        genericDAO g = new genericDAO();
        try {
            g.insertObject(c);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public static void addProduct(int id, String name, int price, String description, int stoc) throws IntrospectionException, SQLException, IllegalAccessException {
        Product p = new Product(id, name, price, description, stoc);
        genericDAO g = new genericDAO();
        try {
            g.insertObject(p);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    public static void addOrder(int clientId, int productId, int quantity) throws IntrospectionException, SQLException, IllegalAccessException {
        int sum = 0;
        sum = getPrice(productId) * quantity;
        Order p = new Order(clientId, productId, quantity, sum);
        genericDAO g = new genericDAO();
        try {
            g.insertObject(p);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private static int getPrice(int productId) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT " + " price " + " FROM tema3db.product" + " WHERE " + "id" + " =" + productId;
        System.out.println(query);
        try {
            connection = databaseCon.connect();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();


        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(resultSet.getRow());
        resultSet.first();
        int next = resultSet.getInt("price");
        return next;
    }
}
