package Model;

public class Order {

    int id;
    int clientId;
    int productId;
    int quantity;
    int sum;

    public Order(int clientId, int productId,int quantity,int sum)
    {
        this.id = (int) (Math.random() * 1000);
        this.clientId = clientId;
        this.productId = productId;
        this.quantity = quantity;
        this.sum = sum;
    }


}
