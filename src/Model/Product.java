package Model;

public class Product {

    int id;
    String name;
    int price;
    String description;
    int stoc;

    public Product(int id,String name, int price,String desc,int stoc)
    {
        this.id = id;
        this.name = name;
        this.price = price;
        description = desc;
        this.stoc = stoc;
    }

}
