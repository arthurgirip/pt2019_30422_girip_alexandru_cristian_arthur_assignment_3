package Presentation;


import Business_Logic.warehouseBLL;
import Data_Access.databaseCon;
import Data_Access.genericDAO;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.sql.*;

public class GUI extends  JFrame{

    static JTable t1 = new JTable();
    static JTable t2 = new JTable();


    static TableModel modell = new TableModel();
public static void executeSQlQuery(String query, String message)
{
    Connection con = databaseCon.connect();
    Statement st;
    try{
        st = con.createStatement();
        if((st.executeUpdate(query)) == 1)
        {
            // refresh jtable data
            DefaultTableModel model = (DefaultTableModel)t2.getModel();
            model.setRowCount(0);

            JOptionPane.showMessageDialog(null, "Data "+message+" Succefully");
        }else{
            JOptionPane.showMessageDialog(null, "Data Not "+message);
        }
    }catch(Exception ex){
        ex.printStackTrace();
    }
}

public static void refreshTable(JTable t,String s)
{
    Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    DefaultTableModel model;

    Connection conn = null;
    if(s=="clients") {
        try {
            con = databaseCon.connect();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery("SELECT * FROM client");

            //warehouseBLL.getClients(rs);

            System.out.println("Connected");
            System.out.println(rs.getRow());

        } catch (SQLException e) {
            e.printStackTrace();
        }

        model = new DefaultTableModel(new String[]{"id", "name", "phone"}, 0);
        try {
            rs = stmt.executeQuery("SELECT * FROM client");
            while (rs.next()) {
                int d = rs.getInt("id");
                String n = rs.getString("name");
                String num = rs.getString("phone");
                model.addRow(new Object[]{d, n, num});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    else {
        try {
            con = databaseCon.connect();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery("SELECT * FROM product");

            //warehouseBLL.getClients(rs);

            System.out.println("Connected");
            System.out.println(rs.getRow());

        } catch (SQLException e) {
            e.printStackTrace();
        }

        model = new DefaultTableModel(new String[]{"id", "name","price", "description","stoc"}, 0);
        try {
            rs = stmt.executeQuery("SELECT * FROM product");
            while (rs.next()) {
                int d = rs.getInt("id");
                String n = rs.getString("name");
                int num = rs.getInt("price");
                String desc = rs.getString("description");
                int stoc = rs.getInt("stoc");
                model.addRow(new Object[]{d, n, num,desc,stoc});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    t.setModel(model);
}

public static void placeOrder()
{
    JFrame f1 = new JFrame();
    f1.setDefaultCloseOperation(EXIT_ON_CLOSE);
    f1.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    f1.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();

    JLabel label_C_ID = new JLabel("Client ID : ");
    JLabel label_P_ID = new JLabel("Product ID : ");
    JLabel label_P_QUANTITY = new JLabel("Quantity : ");

    JTextField text_C_ID = new JTextField(5);
    JTextField text_P_ID = new JTextField(5);
    JTextField text_P_QUANTITY = new JTextField(5);

    JButton button_O_ADD = new JButton();
    button_O_ADD.setText("Send Order");
    button_O_ADD.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                warehouseBLL.addOrder(Integer.parseInt(text_C_ID.getText()),Integer.parseInt(text_P_ID.getText()),Integer.parseInt(text_P_QUANTITY.getText()));
            } catch (IntrospectionException | IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        }
    });

    c.gridy=0;
    c.gridx=0;
    f1.add(label_C_ID,c);

    c.gridy=1;
    c.gridx=0;
    f1.add(label_P_ID,c);

    c.gridy=2;
    c.gridx=0;
    f1.add(label_P_QUANTITY,c);

    c.gridy=0;
    c.gridx=1;
    f1.add(text_C_ID,c);

    c.gridy=1;
    c.gridx=1;
    f1.add(text_P_ID,c);

    c.gridy=2;
    c.gridx=1;
    f1.add(text_P_QUANTITY,c);

    c.gridy++;
    c.gridx--;
    f1.add(button_O_ADD,c);

    f1.setSize(200,200);
    f1.pack();
    f1.setVisible(true);



}

public static void clientsTable()
{
    JFrame f1 = new JFrame();


    Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;

    Connection conn = null;
    try {
        con = databaseCon.connect();
        stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
        rs = stmt.executeQuery("SELECT * FROM client");

        //clientBLL.getClients(rs);

        System.out.println("Connected");
        System.out.println(rs.getRow());

    } catch (SQLException e) {
        e.printStackTrace();
    }


    DefaultTableModel model = new DefaultTableModel(new String[] {"id","name","phone"},0);
    try {
        rs = stmt.executeQuery("SELECT * FROM client");
        while(rs.next())
        {
            int d = rs.getInt("id");
            String n = rs.getString("name");
            String num = rs.getString("phone");
            model.addRow(new Object[]{d,n,num});
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    t1.setModel(model);


    JLabel label_C_NAME;
    JLabel label_C_ID;
    JLabel label_C_NUMBER;

    JTextField textField_C_NAME;
    JTextField textField_C_ID;
    JTextField textField_C_PHONE;

    JButton button_ADD_ROW;
    JButton button_UPDATE_DATABASE;
    JButton button_PLACE_ORDER;

    label_C_NAME = new JLabel();
    label_C_ID = new JLabel();
    label_C_NUMBER = new JLabel();

    textField_C_NAME = new JTextField(10);
    textField_C_ID = new JTextField(10);
    textField_C_PHONE = new JTextField(10);

    button_ADD_ROW = new JButton();
    button_UPDATE_DATABASE = new JButton();
    button_PLACE_ORDER = new JButton();

    label_C_NAME.setText("Client name :");
    label_C_ID.setText("Client ID :");
    label_C_NUMBER.setText("Client Number :");

    textField_C_NAME.setText("Enter new client name");
    textField_C_ID.setText("101");
    textField_C_PHONE.setText("0");

    button_ADD_ROW.setText("Add row to table");
    Statement finalStmt = stmt;
    button_ADD_ROW.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                System.out.println(textField_C_PHONE.getText());
                warehouseBLL.addClient(Integer.parseInt(textField_C_ID.getText()),textField_C_NAME.getText(),textField_C_PHONE.getText());
                t1.setModel(modell.CreateModel(warehouseBLL.getClients()));
                refreshTable(t1,"clients");
            } catch (IntrospectionException | IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
//            String query = "INSERT INTO client(id,name,phone) VALUES ("+Integer.parseInt(textField_C_ID.getText())+",'"+textField_C_NAME.getText()+"','"+ textField_C_PHONE.getText()+"')";
//            executeSQlQuery(query,"Update");
//            refreshTable(t1);

        }
    });

    button_UPDATE_DATABASE.setText("Update database");
    button_UPDATE_DATABASE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            String query = "UPDATE client SET name = '"+textField_C_NAME.getText()+"', phone = '"+textField_C_PHONE.getText()+"' " + " WHERE (`id` = "+textField_C_ID.getText() +")";
            executeSQlQuery(query, "Updated");
            refreshTable(t1,"clients");
        }
    });
    button_PLACE_ORDER.setText("Place Order");
    button_PLACE_ORDER.addActionListener(
            new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    placeOrder();
                }
            });

    JButton button_DELETE = new JButton();
    button_DELETE.setText("Delete");
    button_DELETE.addActionListener(
            new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.out.println(t1.getValueAt(t1.getSelectedRow(),2));
                    String query = "DELETE FROM tema3db.client  WHERE (id = '"+t1.getValueAt(t1.getSelectedRow(),0) + "')";
                    executeSQlQuery(query, "Updated");
                    refreshTable(t1,"clients");
                }
            }
    );



    f1.setDefaultCloseOperation(EXIT_ON_CLOSE);
    f1.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    f1.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    t1.setVisible(true);

    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.CENTER;
    c.weightx = 0.5;
    c.weighty = 1.0;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    f1.add(new JScrollPane(t1), c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_START;
    c.weightx = 0.25;
    c.weighty = 0;
    c.gridx = 0;
    c.gridy = 2;
    c.gridwidth = 1;
    f1.add(label_C_NAME, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_END;
    c.weightx = 0.75;
    c.weighty = 0;
    c.gridx = 1;
    c.gridy = 2;
    c.gridwidth = 1;
    f1.add(textField_C_NAME, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.weightx = 0.25;
    c.weighty = 0;
    c.anchor = GridBagConstraints.LINE_START;
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    f1.add(label_C_ID, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_END;
    c.weightx = 0.75;
    c.weighty = 0;
    c.gridx = 1;
    c.gridy = 1;
    c.gridwidth = 1;
    f1.add(textField_C_ID, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_START;
    c.weightx = 0.25;
    c.weighty = 0;
    c.gridx = 0;
    c.gridy = 3;
    c.gridwidth = 1;
    f1.add(label_C_NUMBER, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_END;
    c.weightx = 0.75;
    c.weighty = 0;
    c.gridx = 1;
    c.gridy = 3;
    c.gridwidth = 1;
    f1.add(textField_C_PHONE, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_START;
    c.weightx = 0.5;
    c.weighty = 0;
    c.gridx = 0;
    c.gridy = 6;
    c.gridwidth = 1;
    f1.add(button_ADD_ROW, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_END;
    c.weightx = 0.5;
    c.weighty = 0;
    c.gridx = 1;
    c.gridy = 6;
    c.gridwidth = 1;
    f1.add(button_UPDATE_DATABASE, c);

    c.fill = GridBagConstraints.HORIZONTAL;
    c.anchor = GridBagConstraints.LINE_START;
    c.weightx = 0.5;
    c.weighty = 0;
    c.gridx = 0;
    c.gridy = 7;
    c.gridwidth = 1;
    f1.add(button_PLACE_ORDER, c);

    c.gridx=1;
    c.gridy=7;
    f1.add(button_DELETE,c);

    f1.setSize(300,300);
    f1.pack();
    f1.setVisible(true);
}
    public static void productsTable()
    {
        JFrame f1 = new JFrame();


        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        Connection conn = null;
        try {
            con = databaseCon.connect();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery("SELECT * FROM product");

            //clientBLL.getClients(rs);

            System.out.println("Connected");
            System.out.println(rs.getRow());

        } catch (SQLException e) {
            e.printStackTrace();
        }


        DefaultTableModel model = new DefaultTableModel(new String[] {"id","name","price","description","stoc"},0);
        try {
            rs = stmt.executeQuery("SELECT * FROM product");
            while(rs.next())
            {
                int d = rs.getInt("id");
                String n = rs.getString("name");
                String num = rs.getString("price");
                String desc = rs.getString("description");
                int stoc = rs.getInt("stoc");
                model.addRow(new Object[]{d,n,num,desc,stoc});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        t2.setModel(model);


        JLabel label_C_NAME = new JLabel();
        JLabel label_C_ID = new JLabel();
        JLabel label_C_NUMBER = new JLabel();
        JLabel label_C_PRICE = new JLabel();
        JLabel label_C_STOC = new JLabel();

        JTextField textField_C_NAME;
        JTextField textField_C_ID;
        JTextField textField_C_DESCRIPTION;
        JTextField textField_C_PRICE;
        JTextField textField_C_STOC;

        JButton button_ADD_ROW;
        JButton button_UPDATE_DATABASE;
        JButton button_PLACE_ORDER;

        textField_C_NAME = new JTextField(10);
        textField_C_ID = new JTextField(10);
        textField_C_DESCRIPTION = new JTextField(10);
        textField_C_PRICE = new JTextField(10);
        textField_C_STOC = new JTextField(10);

        button_ADD_ROW = new JButton();
        button_UPDATE_DATABASE = new JButton();
        button_PLACE_ORDER = new JButton();

        label_C_ID.setText("Product ID :");
        label_C_NAME.setText("Product Name :");
        label_C_NUMBER.setText("Product Desctiprion :");
        label_C_PRICE.setText("Product Price : ");
        label_C_STOC.setText("Product Stoc : ");

        textField_C_NAME.setText("Enter new product name");
        textField_C_ID.setText("101");
        textField_C_DESCRIPTION.setText("...");
        textField_C_PRICE.setText("1");
        textField_C_STOC.setText("1");

        button_ADD_ROW.setText("Add row to table");
        Statement finalStmt = stmt;
        button_ADD_ROW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    warehouseBLL.addProduct(Integer.parseInt(textField_C_ID.getText()),textField_C_NAME.getText(),Integer.parseInt(textField_C_PRICE.getText()),textField_C_DESCRIPTION.getText(),Integer.parseInt(textField_C_STOC.getText()));
                    t2.setModel(modell.CreateModel(warehouseBLL.getClients()));
                    refreshTable(t2,"products");
                } catch (IntrospectionException | IllegalAccessException e1) {
                    e1.printStackTrace();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                //String query = "INSERT INTO product(id,name,description,price,stoc) VALUES ("+Integer.parseInt(textField_C_ID.getText())+",'"+textField_C_NAME.getText()+"','"+ textField_C_DESCRIPTION.getText()+"','"+Integer.parseInt(textField_C_PRICE.getText())+"' )";
                //executeSQlQuery(query,"Update");
                //refreshTable(t2);

            }
        });

        button_UPDATE_DATABASE.setText("Update database");
        button_UPDATE_DATABASE.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String query = "UPDATE product SET name = '"+textField_C_NAME.getText()+"', price = "+Integer.parseInt(textField_C_PRICE.getText())+", description = '"+textField_C_DESCRIPTION.getText()+"', stoc = "+Integer.parseInt(textField_C_STOC.getText())+" WHERE (`id` = "+textField_C_ID.getText() +")";
                executeSQlQuery(query, "Updated");
                refreshTable(t2,"products");
            }
        });
        button_PLACE_ORDER.setText("Place Order");
        button_PLACE_ORDER.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        placeOrder();
                    }
                });

        JButton button_DELETE = new JButton();
        button_DELETE.setText("Delete");
        button_DELETE.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println(t1.getValueAt(t1.getSelectedRow(),2));
                        String query = "DELETE FROM tema3db.product  WHERE (id = '"+t2.getValueAt(t2.getSelectedRow(),0) + "')";
                        executeSQlQuery(query, "Updated");
                        refreshTable(t2,"products");
                    }
                }
        );



        f1.setDefaultCloseOperation(EXIT_ON_CLOSE);
        f1.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        f1.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        t2.setVisible(true);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0.5;
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        f1.add(new JScrollPane(t2), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 0.25;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        f1.add(label_C_NAME, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0.75;
        c.weighty = 0;
        c.gridx = 1;
        c.gridy = 2;
        c.gridwidth = 1;
        f1.add(textField_C_NAME, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.25;
        c.weighty = 0;
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        f1.add(label_C_ID, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0.75;
        c.weighty = 0;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 1;
        f1.add(textField_C_ID, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 0.25;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        f1.add(label_C_NUMBER, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0.75;
        c.weighty = 0;
        c.gridx = 1;
        c.gridy = 3;
        c.gridwidth = 1;
        f1.add(textField_C_DESCRIPTION, c);
///////////////////////////////////////////////////////
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 0.25;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = 1;
        f1.add(label_C_PRICE, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0.75;
        c.weighty = 0;
        c.gridx = 1;
        c.gridy = 4;
        c.gridwidth = 1;
        f1.add(textField_C_PRICE, c);
///////////////////////////////////////////
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 0.25;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth = 1;
        f1.add(label_C_STOC, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0.75;
        c.weighty = 0;
        c.gridx = 1;
        c.gridy = 5;
        c.gridwidth = 1;
        f1.add(textField_C_STOC, c);
    ///////////////    //////////////////////////////////////////////////////
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 0.5;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 6;
        c.gridwidth = 1;
        f1.add(button_ADD_ROW, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0.5;
        c.weighty = 0;
        c.gridx = 1;
        c.gridy = 6;
        c.gridwidth = 1;
        f1.add(button_UPDATE_DATABASE, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 0.5;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 7;
        c.gridwidth = 1;
        f1.add(button_PLACE_ORDER, c);

        c.gridy=7;
        c.gridx=1;
        f1.add(button_DELETE,c);

        f1.setSize(300,300);
        f1.pack();
        f1.setVisible(true);
    }


    public static void main(String[] args)
    {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        System.out.println("Hello");
        //databaseCon con = new databaseCon();
        //con.connect();

        clientsTable();
        productsTable();












    }



}
